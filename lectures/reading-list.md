# Multithreading

## Memory consistency models

- [Memory Barriers: a Hardware View for Software Hackers](http://www.puppetmastertrading.com/images/hwViewForSwHackers.pdf)
- [Memory Models: A Case For Rethinking Parallel Languages and Hardware](https://cacm.acm.org/magazines/2010/8/96610-memory-models-a-case-for-rethinking-parallel-languages-and-hardware/pdf)
- [Go Memory Model](https://golang.org/ref/mem), [Don't be clever](https://golang.org/ref/mem#tmp_1)
- [Java Memory Model](https://docs.oracle.com/javase/specs/jls/se7/html/jls-17.html#jls-17.4)
- [Foundations of the C++ Concurrency Memory Model](http://www.hpl.hp.com/techreports/2008/HPL-2008-56.pdf)
- [Explanation of the Linux-Kernel Memory Consistency Model](https://github.com/torvalds/linux/tree/master/tools/memory-model/Documentation) 
- [The Problem of Programming Language Concurrency Semantics](https://www.cl.cam.ac.uk/~jp622/the_problem_of_programming_language_concurrency_semantics.pdf)
- [Weak memory concurrency in C/C++11](https://youtu.be/mOqu8vGSysc)
- [Weak Memory Consistency](https://people.mpi-sws.org/~viktor/wmc/)
- [Weakly Consistent Concurrency](https://www.cs.tau.ac.il/~orilahav/seminar18/index.html)

## Data race detection

- [ThreadSanitizer – data race detection in practice](https://static.googleusercontent.com/media/research.google.com/en//pubs/archive/35604.pdf)
- [FastTrack: Efficient and Precise Dynamic Race Detection](https://users.soe.ucsc.edu/~cormac/papers/pldi09.pdf)

## Futures

- [Futures and Promises](http://dist-prog-book.com/chapter/2/futures.html)
- [Finagle – Concurrent Programming with Futures](https://twitter.github.io/finagle/guide/Futures.html), [Your Server as a Function](https://monkey.org/~marius/funsrv.pdf)
- [Futures for C++11 at Facebook](https://engineering.fb.com/developer-tools/futures-for-c-11-at-facebook/), [Folly Futures](https://github.com/facebook/folly/blob/master/folly/docs/Futures.md), 
- [Zero-cost futures in Rust](http://aturon.github.io/blog/2016/08/11/futures/), [Designing futures for Rust](http://aturon.github.io/blog/2016/09/07/futures-design/)

## Coroutines

### Stackless
- [Stackless Coroutine in Asio](http://think-async.com/Asio/asio-1.12.2/doc/asio/reference/coroutine.html)
- [On Duff's Device and Coroutines](https://research.swtch.com/duff)

## Asynchronous Programming

- [Асинхронность в программировании](https://habr.com/ru/company/jugru/blog/446562/)
- [Асинхронность: назад в будущее](https://habr.com/ru/post/201826/)
- [Kotlin Coroutines Proposal](https://github.com/Kotlin/KEEP/blob/master/proposals/coroutines.md)
- [Rust's Journey to Async/Await](https://www.youtube.com/watch?v=lJ3NC-R3gSI), [RustLatam 2019 - Without Boats: Zero-Cost Async IO](https://www.youtube.com/watch?v=skos4B5x7qE)

### Async / await

- C#: [Механика asnyc/await в C#](https://habr.com/ru/post/260217/)
- C++: [Asymmetric Transfer](https://lewissbaker.github.io/)
- Kotlin: [Coroutines / Implementation details](https://github.com/Kotlin/KEEP/blob/master/proposals/coroutines.md#implementation-details)
- C++: [Understanding operator co_await](https://lewissbaker.github.io/2017/11/17/understanding-operator-co-await), [Understanding the promise type](https://lewissbaker.github.io/2018/09/05/understanding-the-promise-type)
- Rust: [Async/await RFC](https://github.com/rust-lang/rfcs/blob/master/text/2394-async_await.md)]

#### Syntax
- [What Color is Your Function?](https://journal.stuffwithstuff.com/2015/02/01/what-color-is-your-function/), [Hacker news](https://news.ycombinator.com/item?id=8984648)
- C#: [Asynchrony in C# 5 Part Six: Whither async?](https://docs.microsoft.com/en-us/archive/blogs/ericlippert/asynchrony-in-c-5-part-six-whither-async)
- Rust: [A final proposal for await syntax](https://boats.gitlab.io/blog/post/await-decision/), [Await Syntax Write Up](https://paper.dropbox.com/doc/Await-Syntax-Write-Up--AcIbhZ1tPTCloXb2fmFpBTt~Ag-t9NlOSeI4RQ8AINsaSSyJ)
- Kotlin: [How do you color your functions?](https://medium.com/@elizarov/how-do-you-color-your-functions-a6bb423d936d)

## Structured Concurrency

* [Notes on structured concurrency, or: Go statement considered harmful](https://vorpus.org/blog/notes-on-structured-concurrency-or-go-statement-considered-harmful/)
* [Roman Elizarov – Structured concurrency](https://medium.com/@elizarov/structured-concurrency-722d765aa952)
* [Structured Concurrency Group](https://trio.discourse.group/c/structured-concurrency), [Resources](https://trio.discourse.group/t/structured-concurrency-resources/21)

## Lock-Free data structures

- [Simple, Fast, and Practical Non-Blocking and Blocking Concurrent Queue Algorithms](http://www.cs.rochester.edu/~scott/papers/1996_PODC_queues.pdf)
- [Split-Ordered Lists: Lock-Free Extensible Hash Tables](http://people.csail.mit.edu/shanir/publications/Split-Ordered_Lists.pdf)
- [Cliff Click – A Lock-Free Hash Table](https://www.youtube.com/watch?v=HJ-719EGIts)
- [Цикл статей про lock-free структуры данных](https://habr.com/post/195770/)
- [Data Structures in the Multicore Age](http://people.csail.mit.edu/shanir/publications/p76-shavit.pdf)
 
## Lock-Freedom и Wait-Freedom

- [A Practical Multi-Word Compare-and-Swap Operation](https://www.cl.cam.ac.uk/research/srg/netos/papers/2002-casn.pdf)
- [Wait-Free Synchronization](https://cs.brown.edu/~mph/Herlihy91/p124-herlihy.pdf)
- [On the Nature of Progress](http://citeseerx.ist.psu.edu/viewdoc/download;?doi=10.1.1.469.3698&rep=rep1&type=pdf)
- [Are Lock-Free Concurrent Algorithms Practically Wait-Free?](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/paper-18.pdf)

## Consistency models for concurrent objects

- [Linearizability: A Correctness Condition for Concurrent Objects ](https://cs.brown.edu/~mph/HerlihyW90/p463-herlihy.pdf)
- [Consistency Models Map](https://jepsen.io/consistency)
- [Linearizability versus Serializability](http://www.bailis.org/blog/linearizability-versus-serializability/)
- [Strong consistency models](https://aphyr.com/posts/313-strong-consistency-models)

## Transactions
 
- [ Martin Kleppmann – Transactions: myths, surprises and opportunities](https://www.youtube.com/watch?v=5ZjhNTM8XU8), [слайды и ссылки](https://martin.kleppmann.com/2015/09/26/transactions-at-strange-loop.html)
- [A Critique of ANSI SQL Isolation Levels](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/tr-95-51.pdf)
- [Serializable Isolation for Snapshot Databases](https://ses.library.usyd.edu.au/bitstream/2123/5353/1/michael-cahill-2009-thesis.pdf)
- [What Write Skew Looks Like](https://www.cockroachlabs.com/blog/what-write-skew-looks-like/)
- [A Read-Only Transaction Anomaly Under Snapshot Isolation](https://www.cs.umb.edu/~poneil/ROAnom.pdf)
- [Google Percolator](https://storage.googleapis.com/pub-tools-public-publication-data/pdf/36726.pdf)
- [Serializable Snapshot Isolation in PostgreSQL](https://drkp.net/papers/ssi-vldb12.pdf)
- [PostgreSQL SSI Implementation Notes](https://github.com/postgres/postgres/blob/master/src/backend/storage/lmgr/README-SSI)
- [Demystifying Database Systems: An Introduction to Transaction Isolation Levels](https://fauna.com/blog/introduction-to-transaction-isolation-levels)

## Hardware Transactional Memory

- [Maurice Herlihy – Transactional Memory](https://www.youtube.com/watch?v=ZkUrl8BZHjk), [слайды](http://neerc.ifmo.ru/sptcc/slides/slides-herlihy.pdf)
- [Gil Tene – Understanding Hardware Transactional Memory](https://www.youtube.com/watch?v=0jy4Sc_IY7o)
- [Is Parallel Programming Hard, And, If So, What Can You Do About It?](https://mirrors.edge.kernel.org/pub/linux/kernel/people/paulmck/perfbook/perfbook.html), 17.3
- [Глава 16 – Programming with Intel Transactional Synchronization Extensions](https://software.intel.com/sites/default/files/managed/39/c5/325462-sdm-vol-1-2abcd-3abcd.pdf)
- [Глава 13 – Intel TSX Recommendations](https://software.intel.com/sites/default/files/managed/9e/bc/64-ia-32-architectures-optimization-manual.pdf)
- [TSX Anti-Patterns](https://software.intel.com/en-us/articles/tsx-anti-patterns-in-lock-elision-code)
- [Intel TSX Links](https://software.intel.com/en-us/blogs/2013/06/07/web-resources-about-intelr-transactional-synchronization-extensions)
- [Lock Elision Implementation Guide](https://sourceware.org/glibc/wiki/LockElisionGuide)

## Automatic Memory Management

- [The Garbage Collection Handbook](http://gchandbook.org/)
- [Understanding Java Garbage Collection](https://www.azul.com/files/Understanding_Java_Garbage_Collection_v4.pdf) 

---

- [Getting to Go: The Journey of Go's Garbage Collector](https://blog.golang.org/ismmkeynote)
- [Go Proposal: Eliminate STW stack re-scanning](https://github.com/golang/proposal/blob/master/design/17503-eliminate-rescan.md)
- [Go Garbage Collection Implementation](https://go.hidva.com/src/runtime/mgc.go)
- [Алексей Шипилёв — Shenandoah: сборщик мусора, который смог](https://www.youtube.com/watch?v=CnRtbtis79U)
- [A Unified Theory of Garbage Collection](https://www.cs.virginia.edu/~cs415/reading/bacon-garbage.pdf)
- [Matter, Anti-Matter, and the Unified Theory of Garbage Collection](http://michaelrbernste.in/2013/06/12/matter-anti-matter.html)

# Distributed systems

## Time

- [Time, Clocks and GPS](http://www2.unb.ca/gge/Resources/gpsworld.nov-dec91.corr.pdf)
- [Relativistic Effects in the Global Positioning System](https://www.aapt.org/doorway/TGRU/articles/Ashbyarticle.pdf)

## Replicated R/W Register

- [Notes on Theory of Distributed Computing](http://www.cs.yale.edu/homes/aspnes/classes/465/notes.pdf), глава 16

## Replication, Atomic Broadcast and Consensus

- [Unreliable Failure Detectors for Reliable Distributed Systems](https://www.cs.utexas.edu/~lorenzo/corsi/cs380d/papers/p225-chandra.pdf)
- [Sequential Consistency versus Linearizability](https://pdfs.semanticscholar.org/787c/d132b7c849863393a01ae8f0d92b6a3f8cb3.pdf)

## Impossibility of Consensus

- [Impossibility of Distributed Consensus with One Faulty Process](https://groups.csail.mit.edu/tds/papers/Lynch/jacm85.pdf)
- [Notes on Theory of Distributed Computing](http://www.cs.yale.edu/homes/aspnes/classes/465/notes.pdf), глава 11
- [On the Minimal Synchronism Needed for Distributed Consensus](https://www.cs.huji.ac.il/~dolev/pubs/p77-dolev.pdf)

## Single Decree Paxos

- [Part-Time Parliament](https://lamport.azurewebsites.net/pubs/lamport-paxos.pdf)
- [Paxos Made Simple](https://lamport.azurewebsites.net/pubs/paxos-simple.pdf)
- Заметки Лесли Лампорта про [Part-Time Parliament](http://lamport.azurewebsites.net/pubs/pubs.html#lamport-paxos) и [Paxos Made Simple](http://lamport.azurewebsites.net/pubs/pubs.html#paxos-simple)
- [Notes on Theory of Distributed Computing](http://www.cs.yale.edu/homes/aspnes/classes/465/notes.pdf), глава 12
- [Псевдокод](https://pdos.csail.mit.edu/archive/6.824-2013/notes/paxos-code.html)
- [Неоднозначность](https://stackoverflow.com/questions/29880949/contradiction-in-lamports-paxos-made-simple-paper) в Paxos Made Simple, про которую Лэмпорт пишет в своей [заметке](http://lamport.azurewebsites.net/pubs/pubs.html#paxos-simple)
- [Who are the legislators of Paxos?](https://cs.stackexchange.com/questions/12401/who-are-the-legislators-of-paxos)
- [Flexible Paxos TLA+ Spec](https://github.com/fpaxos/fpaxos-tlaplus/blob/master/FPaxos.tla)

## 6. State Machine Replication, MultiPaxos

- [I Heart Logs / Introduction](https://www.oreilly.com/library/view/i-heart-logs/9781491909379/ch01.html)

### Алгоритмы
- Multi-Paxos: [Paxos Made Moderately Complex](http://www.cs.cornell.edu/courses/cs7412/2011sp/paxos.pdf)
- Zookeeper Atomic Broadcast: [ZAB: High-performance broadcast for primary-backup systems](http://knowably-attachments.s3.amazonaws.com/u/55b69a1ce4b00ab397d67250/7c8734d3cf02154499a9b3161ef9f575/Zab_2011.pdf)
- RAFT: [In Search of an Understandable Consensus Algorithm](https://raft.github.io/raft.pdf)
- VR: [Viewstamped Replication Revisited](http://pmg.csail.mit.edu/papers/vr-revisited.pdf)

### RAFT

- [RAFT Consensus Algorithm](https://raft.github.io/)
- [In Search of an Understandable Consensus Algorithm](https://raft.github.io/raft.pdf)
- [Consensus: Bridging Theory and Practice](https://github.com/ongardie/dissertation)
- [RAFT Lecture](https://www.youtube.com/watch?v=YbZ3zDzDnrw), [slides](https://raft.github.io/slides/raftuserstudy2013.pdf)
- MIT 6.824: [Part 1](https://pdos.csail.mit.edu/6.824/notes/l-raft.txt), [Part 2](https://pdos.csail.mit.edu/6.824/notes/l-raft2.txt)
- [TLA+ Spec](https://github.com/ongardie/raft.tla/blob/master/raft.tla)
- [Instructors' Guide to Raft](https://thesquareplanet.com/blog/instructors-guide-to-raft/)
- [Students' Guide to Raft](https://thesquareplanet.com/blog/students-guide-to-raft/)
- [raft-dev Google Group](https://groups.google.com/forum/#!forum/raft-dev)
- [ectd/raft](https://github.com/etcd-io/etcd/tree/master/raft)

## Formal Methods, TLA+

- [TLA+ Home Page](https://lamport.azurewebsites.net/tla/tla.html)
- [TLA+ Video Course](http://lamport.azurewebsites.net/video/videos.html)
- [If You’re Not Writing a Program, Don’t Use a Programming Language](http://bulletin.eatcs.org/index.php/beatcs/article/view/539/532)
- [What Good is Temporal Logic?](https://www.microsoft.com/en-us/research/uploads/prod/2016/12/What-Good-Is-Temporal-Logic.pdf)
- [TLA+ In Practice and Theory](https://pron.github.io/posts/tlaplus_part1)
- [Learn TLA+](https://learntla.com/introduction/)
- [Dr. TLA+ Series](https://github.com/tlaplus/DrTLAPlus)

### Применения в индустрии

- [Use of Formal Methods at Amazon Web Services](https://lamport.azurewebsites.net/tla/formal-methods-amazon.pdf)
- [Experience of software engineers	using TLA+, PlusCal and TLC](http://tla2012.loria.fr/contributed/newcombe-slides.pdf)

#### Спеки
- [Serializable Snapshot Isolation](https://github.com/pron/amazon-snapshot-spec/blob/master/serializableSnapshotIsolation.tla)
- [Linux Memory Model](https://github.com/aparri/memory-model) – слабые модели памяти
- [Ticket Lock](https://kernel.googlesource.com/pub/scm/linux/kernel/git/cmarinas/kernel-tla/+/master/ticketlock.tla) и [другие ядерные спеки](https://kernel.googlesource.com/pub/scm/linux/kernel/git/cmarinas/kernel-tla/+/master/), [доклад](https://linuxplumbersconf.org/event/2/contributions/60/)
- [PingCAP](https://github.com/pingcap/tla-plus) – репликация, распределенные транзакции
- [Azure CosmosDB](https://github.com/Azure/azure-cosmos-tla) – модели согласованности для облачной БД
- [Kafka Replication Protocol](https://github.com/hachikuji/kafka-specification/blob/master/Kip101.tla), [доклад](https://www.confluent.io/kafka-summit-sf18/hardening-kafka-replication)
