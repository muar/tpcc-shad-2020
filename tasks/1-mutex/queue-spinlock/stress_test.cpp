#include "queue_spinlock.hpp"

#include <twist/test_framework/test_framework.hpp>
#include <twist/strand/test.hpp>

#include <twist/fault/adversary/inject_fault.hpp>

#include <twist/test_utils/executor.hpp>
#include <twist/test_utils/barrier.hpp>

#include <chrono>
#include <thread>
#include <vector>

namespace stress {
  class Tester {
   public:
    Tester(const TTestParameters& parameters)
        : parameters_(parameters),
          start_barrier_(parameters_.Get(0)),
          shared_vars_(parameters_.Get(2), 0) {
    }

    void Run() {
      size_t threads = parameters_.Get(0);

      {
        twist::test_utils::ScopedExecutor executor;

        for (size_t t = 0; t < threads; ++t) {
          executor.Submit(&Tester::RunTestThread, this);
        }
      }

      size_t iterations = parameters_.Get(1);

      size_t expected_value = threads * iterations;
      for (size_t shared_var : shared_vars_) {
        ASSERT_EQ(shared_var, expected_value);
      }
    }

   public:
    void RunTestThread() {
      start_barrier_.PassThrough();

      size_t iterations = parameters_.Get(1);
      for (size_t i = 0; i < iterations; ++i) {
        solutions::QueueSpinLock::Guard guard(queue_spinlock_);
        CriticalSection();
      }
    }

    void CriticalSection() {
      ASSERT_FALSE(in_critical_section_.exchange(true));

      for (auto& shared : shared_vars_) {
        NonAtomicIncrement(shared);
      }

      ASSERT_TRUE(in_critical_section_.exchange(false));
    }

    static void NonAtomicIncrement(size_t& value) {
      static volatile size_t temp;

      temp = value;
      temp = temp + 1;
      twist::fault::InjectFault();
      value = temp;
    }

   private:
    TTestParameters parameters_;

    twist::test_utils::OnePassBarrier start_barrier_;

    std::vector<size_t> shared_vars_;
    std::atomic<bool> in_critical_section_{false};

    solutions::QueueSpinLock queue_spinlock_;
  };
}  // namespace stress


void QueueSpinLockStressTest(TTestParameters parameters) {
  stress::Tester(parameters).Run();
}


// Parameters: threads, iterations per thread, num shared vars

T_TEST_CASES(QueueSpinLockStressTest)
    .TimeLimit(std::chrono::minutes(1))
    .Case({2, 50000, 1})
    .Case({2, 25000, 2})
    .Case({5, 50000, 1})
    .Case({5, 25000, 5})
    .Case({10, 10000, 1})
    .Case({10, 10000, 5});

#if defined(TWIST_FIBER)

// Extra test cases
T_TEST_CASES(QueueSpinLockStressTest)
  .TimeLimit(std::chrono::seconds(30))
  .Case({10, 100000, 3})
  .Case({20, 100000, 2});

#endif

RUN_ALL_TESTS()
