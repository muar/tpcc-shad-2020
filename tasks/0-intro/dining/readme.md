# Обедающие философы

За круглым столом сидят $`n`$ философов. Перед каждым из них стоит тарелка с пастой. Между каждой парой соседних философов лежит вилка.

Каждый философ за столом

1) Берет две лежащие рядом вилки
2) Ест пасту
3) Кладет вилки обратно на стол
4) Думает над чем-то
5) Затем повторяет все сначала

Зачем есть пасту двумя вилками – не ясно, но так устроены философы.

Разумеется, два философа, сидящих рядом, не могут есть одновременно, поскольку они делят между собой одну вилку.

Вам нужно придумать, как философам брать и отпускать вилки, чтобы никто из них не голодал.

---

В нашей задаче философы – это потоки, вилки – мьютексы, а сама задача посвящена понятию [starvation](https://en.wikipedia.org/wiki/Starvation_(computer_science)).

Для решения реализуйте методы `AcquireForks` и `ReleaseForks` в файле `philosopher.cpp`.

Каждый философ знает свое место за столом (поле `seat_`) и вилки слева и справа от себя. Ничем другим он пользоваться не должен.

Для решения задачи нужно
1) Написать наивный код и получить дэдлок
2) Понять, как этот дэдлок устроен
3) Придумать, как его обойти
