# Executors

## Выразительные средства / среда исполнения

[Коллбэки](https://github.com/chriskohlhoff/asio/blob/master/asio/src/examples/cpp14/echo/async_tcp_echo_server.cpp), [фьючи](https://github.com/facebook/folly/blob/master/folly/docs/Futures.md), [файберы](https://gitlab.com/Lipovsky/tinyfiber), [stackless корутины](https://en.cppreference.com/w/cpp/language/coroutines) – это различные _выразительные средства_ асинхронного программирования.

С помощью этих средств разработчик может описать конкурентные активности в своей программе (например, обработку клиентских соединений) без использования блокирующего (поток) ожидания, что в свою очередь позволяет плотно упаковать эти активности в ограниченное число потоков операционной системы.

Но это лишь _выразительные средства_, и каждому из них требуется _среда исполнения_, т.е. набор потоков, которые будут непосредственно исполнять описанные конкурентные активности.

## Tasks (callbacks) are everywhere

С позиции _разработчика_, который пишет прикладной асинхронный код, перечисленные средства выразительности сильно отличаются друг от друга: достаточно сравнить реализацию простейшего сетевого приложения на _Asio_ и на файберах / корутинах.

Но несмотря на все внешние различия, с позиции _среды исполнения_ все эти выразительные средства выглядят _одинаково_: 

И коллбэки _Asio_,  и файберы, и продолжения фьюч – все это _задачи_, обернутые в замыкания _коллбэки_, которые нужно запускать на выделенном наборе потоков. Эти задачи при исполнении генерируют новые задачи, и исполнение тем самым бежит вперед.

## Декомпозиция и унификация

Сделанные выше наблюдения позволяют

1) декомпозировать конкретные _выразительные средства_ от _среды исполнения_ и
2) _унифицировать_ эту среду исполнения в виде фреймворка _экзекуторов_ (_executors_).

## `IExecutor`

Асинхронный код не оперирует понятием потока, вместо этого он оперирует абстракцией _экзекутора_.

Функция экзекутора – исполнять _задачи_ – порции неблокирующего кода (коллбэки асинхронных операций, файберы и т.д.). 

Запуск потоков, очереди и балансировка нагрузки, синхронизация – это внутренняя механика, которую экзекуторы скрывают за своей абстракцией от прикладного разработчика.

Экзекутор предоставляет пользователю единственный метод `Execute(Task&& task)` – запланировать задачу на запуск в некотором потоке.

```cpp
executor->Execute([]() {
  std::cout << "Hello!" << std::endl;
});
```

В дикой природе можно встретить различные альтернативные имена для этого метода: `Submit`, `Schedule`, `Add`.

Экзекутор может владеть потоками и самостоятельно исполнять задачи, а может лишь декорировать другой экзекутор.

### Аргументы задачи и возврат результата

Если в задаче нужно вызвать функцию с аргументами, то следует обернуть такой вызов в замыкание.

Экзекуторы - _one-way_, они не предоставляют механизма для возврата результата из задачи. Эта функциональность не требует усложнения существующей абстракции, она легко реализуется поверх этой абстракции с помощью фьюч:

```cpp
// template <typename T>
// Future<T> AsyncVia(Function<T()> f, IExecutorPtr e)

// Запустить задачу в пуле потоков `thread_pool`
// и вернуть caller-у фьючу, представляющую будущий результат
auto future = AsyncVia([]() -> int { return 42; }, thread_pool);
```

### Примеры

Приведем примеры интеграции экзекуторов и различных асинхронных механизмов:

#### Файберы

_Файбер_ - это сумма следующих слагаемых:
1) _Корутина_
2) _Экзекутор_
3) _Задача_, которая запускается в экзекуторе, резьюмит корутину и перепланирует себя после остановки корутины

```cpp
// Spawn(FiberRoutine body, IExecutorPtr executor);
// Запускаем файбер в экзекуторе `thread_pool` с помощью
Spawn(body, thread_pool);

// Где-то в теле файбера:

// Запускаем новый файбер в текущем экзекуторе
Spawn(child);

// Можем перепрыгнуть в другой пул потоков
TeleportTo(another_thread_pool);
```

#### Asio

В _Asio_ за исполнение коллбэков отвечают потоки-_доноры_, которые вместе образуют _loop executor_:

```cpp
asio::io_context event_loop;

// Заводим таймер
asio::steady_timer timer(event_loop, asio::chrono::seconds(5));
timer.async_wait(callback)

// Присоединяемся к пулу потоков, исполняющих запланированные коллбэки
event_loop.run();
```

#### Фьючи

Фьючи представляют собой клей, с помощью которого можно выстраивать задачи в пайплайны и графы.

Чтобы "передать" фьючу в следующую по цепочке вызовов функцию, нужно с помощью `via` явно указать экзекутор, в котором эта функция будет вызвана:

```cpp
// Cм. https://github.com/facebook/folly/blob/master/folly/docs/Futures.md
// Когда фьюча `f` получит значение, то оно будет передано функции `foo`
// которая будет вызвана в экзекуторе `e`
std::move(f).via(e).thenValue(foo).thenValue(bar);
```

## Thread Pool

Базовый экзекутор – пул из фиксированного числа потоков, разбирающих общую очередь задач.

```cpp
// Создаем пул из 4-х потоков, потоки пула маркируем именем "pool";
auto thread_pool = MakeStaticThreadPool(4, "pool")

// Планируем задачу
thread_pool->Execute([]() {
  ExpectThread("pool");  // проверяем, что работаем в нужном пуле
  DoWork();
});
```

### Легкие и тяжелые задачи

Реализация пула зависит от природы задач, которые в нем будут исполняться.

Если пул используется для запуска тяжелых вычислительных задач, то простой реализации с общей очередью будет достаточно. 

Если же пул используется как планировщик для файберов, т.е. для очень коротких задач, которые еще и интенсивно коммуницируют между собой с помощью каналов и примитивов синхронизации, то затраты на синхронизацию на общей очереди будут слишком заметны на фоне самих задач. 

Эффективная реализация пула для файберов должна поддерживать тред-локальные очереди и механизм балансировки нагрузки между ними.

## Strand (Serial Executor)

Запускаемым в пуле задачам может потребоваться синхронизация. Использовать мьютексы нельзя: на время ожидания мьютекса задача заблокирует поток пула для других задач, которые лежат в очереди и готовы исполняться. 

Цель асинхронности – избавиться от блокирующего (поток) ожидания, поэтому экзекуторы предоставляют эффективную альтернативу – _strand_-ы.

_Strand_ (или _serial executor_) – экзекутор, запускающий все задачи строго _последовательно в порядке их добавления_ (в том же смысле, как упорядочивает блокирующая очередь).

См. [Strands: Use Threads Without Explicit Locking](http://think-async.com/Asio/asio-1.16.0/doc/asio/overview/core/strands.html)

### Декоратор

Легко представить наивную реализацию strand-а: отдельный поток, разбирающий собственную очередь задач. Но такая реализация будет неэффективна: например, в серверном приложении на _Asio_ strand может создаваться на каждое соединение с клиентом.

Мы хотим, чтобы число потоков в приложении не зависило от числа конкурентных активностей. 

Поэтому strand - это декоратор, он не имеет собственных потоков для исполнения задач, он оборачивает произвольный экзекутор и запускает все задачи в нем:

```cpp
// Создаем strand, который будет исполнять задачи в потоках пула `tp`
auto strand = MakeStrand(tp);
```

Strand ничего не знает про число потоков, которые оказались в его распоряжении, и может запускать свои задачи в разных потоках, но выполняться эти задачи должны строго последовательно.

### Эффективность

Хорошая реализация strand-а с батчингом и лок-фри будет эффективнее мьютекса: критические секции исполняются сериями, что снижает накладные расходы на синхронизацию и дает локальность по кэшу.

### Примеры применения

#### Взаимное исключение для файберов

Пусть разные файберы хотят обращаться к разделяемой структуре данных. Файберы могут запускаться на разных потоках пула, а значит при обращении к разделяемому состоянию им потребуется синхронизация.

Сложное решение – написать concurrent-версию структуры данных, с которой можно работать из разных потоков.

Простое решение – связать с этой структурой данных отдельный strand и телепортироваться в него при обращении:

```cpp
{
  // Телепортируемся в strand, связанный со структурой данных
  auto guard = Within(strand);
  
  // Выполняем операции над ней
  
} // Телепортируемся в исходный экзекутор
```

## Выключение

Пул потоков (execution context) можно остановить с помощью методов `Join` или `Shutdown`:

- `Join` - мягкая остановка, дожидаемся, когда у пула закончится работа, и останавливаем потоки

- `Shutdown` – жесткая остановка, выбрасываем все невыполненные задачи и останавливаем потоки

После остановки новые задачи пулом игнорируются.

## KeepWorking

Пусть мы хотим синхронно дождаться завершения запланированных в пул задач.

Если задача планируется непосредственно через вызов `Execute`, то пул знает о ней и не остановится, пока не выполнит ее.

Но задача может быть _отложенной_:
- Продолжение, которое прицепили к фьюче с помощью `f.Then(foo)`
- Файбер, заблокированный на IO или примитиве синхронизации.

Эти задачи логически запланированы, но вызов `Execute` на ассоциированном с ними экзекуторе отложен до реализации некоторого события.

Поэтому экзекутор предоставляет пользователю механизм, с помощью которого можно анонсировать отложенную работу: методы `WorkCreated` и `WorkCompleted`. 

Напрямую их вызывать не нужно, вместо этого следует обернуть экзекутор, через который будет запущена отложенная задача, в декоратор `KeepWorking`. Этот декоратор на время своей _жизни_ продлит время _работы_ пула потоков.

Таким образом, _outstanding work_ для пула складывается из следующих слагаемых:
- Число запланированных через `Execute`, но еще не выполненных задач
- Число живых `KeepWorking` декораторов

Эту идею вы можете обнаружить в дизайне экзекуторов из _Asio_.

### Пример

```cpp
// tp1, tp2 - два пула потоков

// Не даем пулу `tp1` остановиться до запуска в нем задачи
tp2->Execute([tp1 = KeepWorking(tp1)]() {
  tp1->Execute([]() {
    std::cout << "Hello, World!" << std::endl;
  });
});

// Ждем печати на экран
tp1->Join();
```

### Work / Life

Возможность синхронно дождаться завершения запланированных задач приводит к разделению двух понятий: _времени жизни_ и _времени работы_.

Именно по этой причине возникают два параллельных похожих механизма.

## Unified Executors for C++

- [A Unified Executors Proposal for C++](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2020/p0443r12.html) – будущий дизайн экзекуторов в C++
- [Executors Bibilography](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2020/p0443r12.html#appendix-executors-bibilography)
- [A Universal Async Abstraction for C++](https://cor3ntin.github.io/posts/executors/)



## Задание

1) Реализуйте пул потоков: `static_thread_pool.cpp`
2) Реализуйте strand: `strand.cpp`


## Замечания по реализации

### Thread Pool

Релизуйте простой пул потоков фиксированного размера с блокирующей очередью задач.

Маркируйте потоки пула с помощью переданного в фабричный метод имени.

### Strand

Strand реализует взаимное исключение, а значит внутри него будет спрятан мьютекс. 

Поток, непосредственно исполняющий задачи strand-а, не должен при этом держать мьютекс.

Реализация не должна на бесконечное время захватывать поток пула, который в конечном счете будет исполнять критические секции.

#### Батчинг

Группируйте задачи strand-а в пакеты. Число задач, которые strand отправляет в декорируемый экзекутор не должно зависеть от интенсивности добавления задач в сам strand.

При хорошей реализации strand-а накладные расходы на синхронизацию при росте нагрузки только _снижаются_, а не возрастают, как при использовании мьютексов.

Не используйте `MPMCBlockingQueue`: 1) в strand-е не предполагается никакого блокирующего ожидания 2) интерфейс блокирующей очереди сам по себе противоречит идее батчинга.

#### Lock-Free Queue

От мьютекса, который защищает очередь задач в strand-е, можно избавиться, используя лок-фри очередь. 

В общем случае использование лок-фри контейнеров требует нетривиального механизма управления памятью для решения ABA, да и сама очередь – не слишком простой в реализации контейнер. Но в случае со strand-ом все оказывается гораздо проще: достаточно будет простого лок-фри стека и умения из двух стеков собрать очередь.

С лок-фри очередью синхронизация в strand-е сведется практически к одной атомарной операции как при планировании задачи, так и при захвате целой пачки задач при непосредственном исполнении.
